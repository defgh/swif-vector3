import Foundation

public class Vector3: Comparable, CustomStringConvertible {
    let x: Double
    let y: Double
    let z: Double

    public init(x: Double, y: Double, z: Double) {
        self.x = x
        self.y = y
        self.z = z
    }

    public var description: String {
        return "Vector (x: \(x), y: \(y), z: \(z))"
    }

    public var squareMagnitude: Double {
        return x * x + z * z + y * y
    }

    public var magnitude: Double {
        return squareMagnitude.squareRoot()
    }

    public static func + (lhs: Vector3, rhs: Vector3) -> Vector3 {
        return Vector3(
            x: lhs.x + rhs.x,
            y: lhs.y + rhs.y,
            z: lhs.z + rhs.z
        )
    }

    public static func - (lhs: Vector3, rhs: Vector3) -> Vector3 {
        return Vector3(
            x: lhs.x - rhs.x,
            y: lhs.y - rhs.y,
            z: lhs.z - rhs.z
        )
    }

    public static func * (lhs: Vector3, rhs: Double) -> Vector3 {
        return Vector3(
            x: lhs.x * rhs,
            y: lhs.y * rhs,
            z: lhs.z * rhs
        )
    }

    public static func * (lhs: Double, rhs: Vector3) -> Vector3 {
        return rhs * lhs
    }

    public static func / (lhs: Vector3, rhs: Double) -> Vector3 {
        return Vector3(
            x: lhs.x / rhs,
            y: lhs.y / rhs,
            z: lhs.z / rhs
        )
    }

    public static func < (lhs: Vector3, rhs: Vector3) -> Bool {
        return lhs.squareMagnitude < rhs.squareMagnitude
    }

    public static func == (lhs: Vector3, rhs: Vector3) -> Bool {
        return (lhs.x == rhs.x) && (lhs.y == rhs.y) && (lhs.z == rhs.z)
    }

    public static let zero = Vector3(x: 0, y: 0, z: 0)
}
