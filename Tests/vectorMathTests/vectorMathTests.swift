@testable import vectorMath
import XCTest

final class vectorMathTests: XCTestCase {
    func testInit() throws {
        let v1 = Vector3(x: 1, y: 2, z: 3)
        let zero = Vector3.zero

        XCTAssertEqual(v1.x, 1)
        XCTAssertEqual(v1.y, 2)
        XCTAssertEqual(v1.z, 3)
        XCTAssertEqual(zero.x, 0)
    }

    func testAddition() throws {
        let v1 = Vector3(x: 1, y: 2, z: 3)
        let v2 = Vector3(x: 2, y: 4, z: 6)
        let v3 = v1 + v2

        XCTAssertEqual(v3.x, 3)
        XCTAssertEqual(v3.y, 6)
        XCTAssertEqual(v3.z, 9)
    }

    func testSubstract() throws {
        let v1 = Vector3(x: 1, y: 2, z: 3)
        let v2 = Vector3(x: 2, y: 4, z: 6)
        let v3 = v1 - v2

        XCTAssertEqual(v3.x, -1)
        XCTAssertEqual(v3.y, -2)
        XCTAssertEqual(v3.z, -3)
    }

    func testSquareMagnitude() throws {
        let v1 = Vector3(x: 2, y: 3, z: 6)

        XCTAssertEqual(v1.squareMagnitude, 49)
    }

    func testMagnitude() throws {
        let v1 = Vector3(x: 2, y: 3, z: 6)

        XCTAssertEqual(v1.magnitude, 7)
    }

    func testMultiplication() throws {
        let v1 = Vector3(x: 2, y: 3, z: 6)
        let v2 = v1 * 2
        let v3 = 2 * v2

        XCTAssertEqual(v2.x, 4)
        XCTAssertEqual(v2.y, 6)
        XCTAssertEqual(v2.z, 12)

        XCTAssertEqual(v3.x, 8)
        XCTAssertEqual(v3.y, 12)
        XCTAssertEqual(v3.z, 24)
    }

    func testDivision() throws {
        let v1 = Vector3(x: 2, y: 3, z: 6)
        let v2 = v1 / 2

        XCTAssertEqual(v2.x, 1)
        XCTAssertEqual(v2.y, 1.5)
        XCTAssertEqual(v2.z, 3)
    }

    func testCompare() throws {
        let v1 = Vector3(x: 2, y: 3, z: 6)
        let v2 = Vector3(x: 2, y: 3, z: 6)
        let v3 = Vector3(x: 3, y: 3, z: 6)

        XCTAssertEqual(v1, v2)
        XCTAssertEqual(v1 < v3, true)
    }

    func testDescription() throws {
        let v1 = Vector3(x: 2, y: 3, z: 6)

        XCTAssertEqual("\(v1)", "Vector (x: 2.0, y: 3.0, z: 6.0)")
    }
}
